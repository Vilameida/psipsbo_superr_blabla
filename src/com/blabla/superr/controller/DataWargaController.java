/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.blabla.superr.controller;

import com.blabla.superr.model.data.DataWargaModel;
import com.blabla.superr.model.pojo.DataWarga;
import java.sql.SQLException;
import java.util.List;

/**
 *
 * @author pandu
 */
public class DataWargaController {
    public List<DataWarga> loadDataWargas() throws SQLException {
        DataWargaModel model = new DataWargaModel();
        return model.loadDataWargas();
    }
    
    public int insert(DataWarga dw) throws SQLException{
        DataWargaModel model = new DataWargaModel();
        return model.save(dw);
    }
    
    public int delete(DataWarga dw) throws SQLException{
        DataWargaModel model = new DataWargaModel();
        return model.Delete(dw);
    }
    
    public int update(DataWarga dw) throws SQLException{
        DataWargaModel model = new DataWargaModel();
        return model.update(dw);
    }
    
    public List<DataWarga> search(String cari) throws SQLException{
        DataWargaModel model = new DataWargaModel();
        return model.searchDataWargas(cari);
    }
}
