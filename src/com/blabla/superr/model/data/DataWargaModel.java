/*

 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.blabla.superr.model.data;

import com.blabla.superr.model.pojo.DataWarga;
import com.blabla.superr.utilities.DatabaseUtilities;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author pandu
 */
public class DataWargaModel {

    public List<DataWarga> loadDataWargas() throws SQLException {
        List<DataWarga> wargaList;
        Connection con = DatabaseUtilities.getConnection();
        try {
            Statement state = con.createStatement();
            ResultSet rs = state.executeQuery("SELECT * FROM warga");
            wargaList = new ArrayList<>();
            while (rs.next()) {
                DataWarga dw = new DataWarga();
                dw.setNik(rs.getString("nik"));
                dw.setNama(rs.getString("nama"));
                dw.setTtl(rs.getString("ttl"));
                dw.setJeniskl(rs.getString("jeniskl"));
                dw.setAgama(rs.getString("agama"));
                dw.setPekerjaan(rs.getString("pekerjaan"));
                dw.setStatusprk(rs.getString("statusprk"));
                dw.setKerwarganegaraan(rs.getString("kewarganegaraan"));
                dw.setAlamat(rs.getString("alamat"));

                wargaList.add(dw);
            }
        } finally {
            if (con != null) {
                con.close();
            }
        }
        return wargaList;
    }

    public List<DataWarga> searchDataWargas(String cari) throws SQLException {
        List<DataWarga> wargaList;
        Connection con = DatabaseUtilities.getConnection();
        try {
            Statement state = con.createStatement();
            ResultSet rs = state.executeQuery("SELECT * FROM warga WHERE nik LIKE '%" + cari + "%' OR nama LIKE '%" + cari + "%'");
            wargaList = new ArrayList<>();
            while (rs.next()) {
                DataWarga dw = new DataWarga();
//                System.out.println(rs.getString("nik"));
                dw.setNik(rs.getString("nik"));
                dw.setNama(rs.getString("nama"));
                dw.setTtl(rs.getString("ttl"));
                dw.setJeniskl(rs.getString("jeniskl"));
                dw.setAgama(rs.getString("agama"));
                dw.setPekerjaan(rs.getString("pekerjaan"));
                dw.setStatusprk(rs.getString("statusprk"));
                dw.setKerwarganegaraan(rs.getString("kewarganegaraan"));
                dw.setAlamat(rs.getString("alamat"));

                wargaList.add(dw);
            }
        } finally {
            if (con != null) {
                con.close();
            }
        }
        System.out.println(wargaList.toString());
        return wargaList;
    }

    
    public int save(DataWarga dw) throws SQLException {
        Connection con = DatabaseUtilities.getConnection();
        try {
            PreparedStatement stat = con.prepareStatement("INSERT INTO warga values (?, ?, ?, ?, ?, ?, ?, ?, ?)");
            stat.setString(1, dw.getNik());
            stat.setString(2, dw.getNama());
            stat.setString(3, dw.getTtl());
            stat.setString(4, dw.getJeniskl());
            stat.setString(5, dw.getAgama());
            stat.setString(6, dw.getPekerjaan());
            stat.setString(7, dw.getStatusprk());
            stat.setString(8, dw.getKerwarganegaraan());
            stat.setString(9, dw.getAlamat());
            return stat.executeUpdate();
        } finally {
            if (con != null) {
                con.close();
            }
        }
    }

    public int Delete(DataWarga dw) throws SQLException {
        Connection con = DatabaseUtilities.getConnection();
        try {
            PreparedStatement stat = con.prepareStatement("DELETE FROM warga WHERE nik =?");
            stat.setString(1, dw.getNik());
            return stat.executeUpdate();
        } finally {
            if (con != null) {
                con.close();
            }
        }
    }

    public int update(DataWarga dw) throws SQLException {
        Connection con = DatabaseUtilities.getConnection();
        try {
            PreparedStatement stat = con.prepareStatement("UPDATE warga SET nik=?, nama=?, ttl=?, jeniskl=?, agama=?, pekerjaan=?, statusprk=?, kewarganegaraan=?, alamat=? WHERE nik=?");
            stat.setString(1, dw.getNik());
            stat.setString(2, dw.getNama());
            stat.setString(3, dw.getTtl());
            stat.setString(4, dw.getJeniskl());
            stat.setString(5, dw.getAgama());
            stat.setString(6, dw.getPekerjaan());
            stat.setString(7, dw.getStatusprk());
            stat.setString(8, dw.getKerwarganegaraan());
            stat.setString(9, dw.getAlamat());
            stat.setString(10, dw.getNik());
            return stat.executeUpdate();
        } finally {
            if (con != null) {
                con.close();
            }
        }
    }
}
