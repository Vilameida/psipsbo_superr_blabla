/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.blabla.superr.model.pojo;

/**
 *
 * @author pandu
 */
public class DataWarga {

    private String nik;
    private String nama;
    private String ttl;
    private String jeniskl;
    private String pekerjaan;
    private String agama;
    private String statusprk;
    private String kerwarganegaraan;
    private String alamat;

    public DataWarga() {
    }

    public DataWarga(String nik, String nama, String ttl, String jeniskl, String pekerjaan, String agama,
            String statusprk, String kerwarganegaraan, String alamat) {
        this.nik = nik;
        this.nama = nama;
        this.ttl = ttl;
        this.jeniskl = jeniskl;
        this.pekerjaan = pekerjaan;
        this.agama = agama;
        this.statusprk = statusprk;
        this.kerwarganegaraan = kerwarganegaraan;
        this.alamat = alamat;

    }

    public String getNik() {
        return nik;
    }

    public void setNik(String nik) {
        this.nik = nik;
    }

    public String getNama() {
        return nama;
    }

    public void setNama(String nama) {
        this.nama = nama;
    }

    public String getTtl() {
        return ttl;
    }

    public void setTtl(String ttl) {
        this.ttl = ttl;
    }

    public String getJeniskl() {
        return jeniskl;
    }

    public void setJeniskl(String jeniskl) {
        this.jeniskl = jeniskl;
    }

    public String getPekerjaan() {
        return pekerjaan;
    }

    public void setPekerjaan(String pekerjaan) {
        this.pekerjaan = pekerjaan;
    }

    public String getAgama() {
        return agama;
    }

    public void setAgama(String agama) {
        this.agama = agama;
    }

    public String getStatusprk() {
        return statusprk;
    }

    public void setStatusprk(String statusprk) {
        this.statusprk = statusprk;
    }

    public String getKerwarganegaraan() {
        return kerwarganegaraan;
    }

    public void setKerwarganegaraan(String kerwarganegaraan) {
        this.kerwarganegaraan = kerwarganegaraan;
    }

    public String getAlamat() {
        return alamat;
    }

    public void setAlamat(String alamat) {
        this.alamat = alamat;
    }

}
